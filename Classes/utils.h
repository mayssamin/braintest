#ifndef __UTILS_H__
#define __UTILS_H__


#include <string>
#include <iostream>
#include <sstream>
using namespace std;

string to_string_with_precision(const float a_value, const int percision = 2);

#endif
